import Vue from "vue";
import Vuetify from "vuetify/lib/framework";


Vue.use(Vuetify, {
  rtl: true
});

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#DE1D9D", 
        "primary-md": "#f8d2eb",
        "primary-li": "#fbe8f5",
        secondary: "#212CFF",
        "secondary-md": "#d3d5ff",
        "secondary-li": "#e8e9ff",
        grey: "#939393",
        "grey-md": "#dadada",
        "grey-li": "#f7f8f9",
        "error-li": "#fae1e4",
        error: "#d02c47",
        "success-li": "#f1fff2",
        success: "#4bae4f"
      },
    },
  },
});
