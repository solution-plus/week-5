import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import i18n from "./plugins/i18n";
import axios from "./plugins/axios";
import vuetify from "./plugins/vuetify";
import main from "./assets/scss/main.scss";
import veeValidate from "./plugins/veeValidate";

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;

new Vue({
  router,
  store,
  vuetify,
  i18n,
  main,
  veeValidate,
  render: (h) => h(App),
}).$mount("#app");
